# syntax = docker/dockerfile:1

# Make sure RUBY_VERSION matches the Ruby version in .ruby-version and Gemfile
ARG RUBY_VERSION=3.2.2
FROM registry.docker.com/library/ruby:$RUBY_VERSION-slim
WORKDIR /utility

# Install packages needed to build gems
RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y build-essential git libvips pkg-config

RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y cron 

COPY . .
RUN gem install bunny

COPY scripts/cronjob /etc/cron.d/wipe-old-wads-cronjob
RUN chmod 0644 /etc/cron.d/wipe-old-wads-cronjob && crontab /etc/cron.d/wipe-old-wads-cronjob

RUN mkdir ~/.config && mkdir ~/.config/obsidian

RUN chmod +x ./scripts/*

RUN apt remove build-essential git libvips pkg-config -y --purge --auto-remove
RUN rm -rf /var/lib/apt/lists /var/cache/apt/archives

# Start the server by default, this can be overwritten at runtime
CMD ["/utility/scripts/startup_container.sh"]
