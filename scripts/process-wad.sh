#!/bin/bash -e

cd /obsidian
/obsidian/obsidian -b /obsidian/$1/$2 -l /obsidian/$1/$2.cfg.txt .&>/dev/null
