require 'bunny'

connection = Bunny.new(automatically_recover: false, host: ENV.fetch("RABBIT_HOST", "doom-wotm-rabbitmq-1"), port: 5672, user: ENV.fetch("RABBITMQ_DEFAULT_USER", "user"), pass: ENV.fetch("RABBITMQ_DEFAULT_PASS", "password"))
connection.start

wad_request_channel = connection.create_channel
wad_request_queue = wad_request_channel.queue('wad-requests', durable: true)

created_wads_channel = connection.create_channel
created_wads_queue = created_wads_channel.queue('wad-completed-jobs', durable: true)

wad_request_channel.prefetch(1)
puts ' [*] Waiting for new WAD requests'
 
begin
    wad_request_queue.subscribe(manual_ack: true, block: true) do |delivery_info, _properties, body|
        begin
            if !body or !body.lines or !body.lines[0]
                puts "[!] rabbit mq worker received invalid message body"
                puts body
                wad_request_channel.ack(delivery_info.delivery_tag)
            else
                @wadName = body.lines[0].split(' ')[1]
                puts " [x] Received new Wad Request: '#{@wadName}'"
                IO.write("/obsidian/wads/#{@wadName}.cfg.txt", body)
                `./scripts/process-wad.sh wads #{@wadName}`
                puts " [x] Created  #{@wadName}.pk3"
                
                created_wads_channel.default_exchange.publish(body, routing_key: created_wads_queue.name)
                wad_request_channel.ack(delivery_info.delivery_tag)
            end
        rescue StandardError => e
            puts "[!!] rabbit mq worker exception below"
            puts e
        end
    end
rescue Interrupt => _
    connection.close
end


