wadNames=["Doom1", "Ultimate-Doom", "Doom2", "TnT", "Plutonia"]
sourceWads=["doom1", "ultdoom", "doom2", "tnt", "plutonia"]
gameLength=["single", "few", "episode", "game"]
healthAndAmmo=["none", "scarce", "less", "bit_less", "normal", "bitMore", "more", "heaps"]
itemFreq=["none", "rare", "less", "normal", "more", "heaps"]
secretsBonus=["none", "more", "heaps", "heapser", "heapsest"]
otherOptions=["none", "rare", "few", "less", "some", "more", "heaps", "mixed"]

rng = Random.new

wotmSettings="-- GAME_NAME-WOTM
-- formType = advanced

---- Game Settings ----

    engine = idtech_1
    game = GAME_WAD
    port = zdoom
    length = #{gameLength[rng.rand(0...4)]}
    theme = original

---- Architecture ----

    float_size = #{rng.rand(15...72)}
    bool_prebuilt_levels = 1
    float_overall_lighting_mult = #{rng.rand(0.75...1.25)}
    outdoors = #{otherOptions[rng.rand(0...8)]}
    caves = #{otherOptions[rng.rand(0...8)]}
    liquids = #{otherOptions[rng.rand(0...8)]}
    hallways = #{otherOptions[rng.rand(0...8)]}
    teleporters = #{otherOptions[rng.rand(0...8)]}
    zdoom_vista = disable
    zdoom_skybox = disable

---- Combat ----

    float_mons = #{rng.rand(1..3)}
    float_strength = #{rng.rand(0.5..3.0)}
    bool_pistol_starts = 1
    bool_quiet_start = 0
    mon_variety = #{otherOptions[rng.rand(0...8)]}
    traps = #{otherOptions[rng.rand(0...8)]}
    secret_monsters = no

---- Pickups ----

    health = #{healthAndAmmo[rng.rand(0...8)]}
    ammo = #{healthAndAmmo[rng.rand(0...8)]}
    items = #{itemFreq[rng.rand(0...6)]}
    secrets = #{otherOptions[rng.rand(0...8)]}
    secrets_bonus = #{secretsBonus[rng.rand(0...5)]}

---- Other Modules ----

    @doom_mon_control = 1
      float_zombie = #{rng.rand(0.0..2.5)}
      float_shooter = #{rng.rand(0.0..2.5)}
      float_gunner = #{rng.rand(0.0..2.5)}
      float_ss_nazi = #{rng.rand(0.0..2.5)}
      float_imp = #{rng.rand(0.0..2.0)}
      float_skull = #{rng.rand(0.0..2.0)}
      float_demon = #{rng.rand(0.0..1.5)}
      float_spectre = #{rng.rand(0.0..1.5)}
      float_pain = #{rng.rand(0.0..1.0)}
      float_caco = #{rng.rand(0.0..1.0)}
      float_knight = #{rng.rand(0.0..1.0)}
      float_revenant = #{rng.rand(0.0..1.0)}
      float_mancubus = #{rng.rand(0.0..1.0)}
      float_arach = #{rng.rand(0.0..1.0)}
      float_vile = #{rng.rand(0.0..1.0)}
      float_baron = #{rng.rand(0.0..1.0)}
      float_Cyberdemon = #{rng.rand(0.0..1.0)}
      float_Spiderdemon = #{rng.rand(0.0..1.0)}"

oldWotmData = Dir["/obsidian/wotm/*"]
for i in 0 ... oldWotmData.length do
    File.delete(oldWotmData[i])
end

for i in 0...5 do
    File.open("/obsidian/wotm/#{wadNames[i]}-WOTM.cfg.txt", 'w') { |file| file.write(wotmSettings.sub("GAME_NAME", wadNames[i]).sub("GAME_WAD", sourceWads[i])) }

    # invodke process-wad.sh
    `./scripts/process-wad.sh wotm #{wadNames[i]}-WOTM`
end